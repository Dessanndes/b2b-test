from django.views.generic import (
    TemplateView as DjangoTemplateView,
    ListView as DjangoListView,
    DetailView as DjangoDetailView
)
from django.views.generic.base import ContextMixin


class TitleMixin(ContextMixin):
    title = None

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.get_title()
        return context


class TemplateView(TitleMixin, DjangoTemplateView):
    pass


class ListView(TitleMixin, DjangoListView):
    pass


class Detailview(TitleMixin, DjangoDetailView):
    pass