from django.db import models


class Product(models.Model):
    TYPE_CHOICES = (
        ('Первый тип', 'Первый тип'),
        ('Второй тип', 'Второй тип'),
        ('Третий тип', 'Третий тип'),
    )

    title = models.CharField(max_length=255 ,verbose_name='Назвние')
    description = models.TextField(verbose_name='Описаине')
    images = models.ManyToManyField('ProductImage', blank=True, verbose_name='Изображения')
    cost = models.IntegerField(verbose_name='Цена в рублях')
    type = models.CharField(max_length=255, choices=TYPE_CHOICES, verbose_name='Тип')
    in_stock = models.BooleanField(default=True, verbose_name='На складе')

    class Meta:
        verbose_name = 'продукт'
        verbose_name_plural = 'продукты'

    def __str__(self):
        return self.title


class ProductImage(models.Model):
    image = models.ImageField(upload_to='products', verbose_name='Изобржение')

    class Meta:
        verbose_name = 'изображение продукта'
        verbose_name_plural = 'изображения продукта'

    def __str__(self):
        return self.image.name
