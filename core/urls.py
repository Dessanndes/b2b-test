from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from core import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^product/$', views.product_list, name='product_list'),
    url(r'^product/(?P<id>\d+)/$', views.product, name='product'),
    url(r'^contacts/$', views.contacts, name='contacts'),

    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
