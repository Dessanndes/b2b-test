from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.urls import reverse_lazy
from django.views.generic.edit import FormMixin

import core.generic

from core import models
from core import forms


class IndexView(core.generic.TemplateView):
    title = 'Главная'
    template_name = 'core/index.html'
index = IndexView.as_view()


class ContactsView(FormMixin, core.generic.TemplateView):
    success_url = reverse_lazy('contacts')
    title = 'Контакты'
    template_name = 'core/contacts.html'
    form_class = forms.Contact

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        return self.form_valid(form) if form.is_valid() else self.form_invalid(form)

    def form_valid(self, form):
        email = form.cleaned_data.get('email', None)
        if email:
            send_mail(
                'b2b Test',
                'спасибо, мы скоро с вами свяжемся',
                settings.EMAIL_HOST_USER,
                [email, ],
            )
        send_mail(
            'b2b Test',
            'пользователь {0} написал : "{1}"'.format(
                form.cleaned_data['name'], form.cleaned_data['message']
            ),
            settings.EMAIL_HOST_USER,
            [settings.MANAGER_EMAIL, ],
        )

        messages.info(self.request, 'Ваше сообщение успешно отправлено')
        return super().form_valid(form)

contacts = ContactsView.as_view()


class ProductListView(core.generic.ListView):
    title = 'Товары'
    template_name = 'core/product_list.html'
    model = models.Product
product_list = ProductListView.as_view()


class ProductView(core.generic.Detailview):
    template_name = 'core/product.html'
    model = models.Product
    pk_url_kwarg = 'id'

    def get_title(self):
        return self.get_object()
product = ProductView.as_view()