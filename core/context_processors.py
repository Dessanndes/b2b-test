from django.urls import reverse


def menu(request):

    def get_menu_item(label, url):
        active_class = 'active' if (request.path.startswith(url) and url != '/') or (url == request.path == '/') else ''
        return [label, url, active_class]

    return {
        'menu': [
            get_menu_item('Главная', reverse('index')),
            get_menu_item('Товары', reverse('product_list')),
            get_menu_item('Контакты', reverse('contacts'))
        ]
    }