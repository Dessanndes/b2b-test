from django import forms


class Contact(forms.Form):
    email = forms.EmailField(label='Электронная почта', required=False)
    name = forms.CharField(label='Имя')
    message = forms.CharField(label='Ваше сообщение', widget=forms.Textarea)
